; ioctl requests
#define GETDAVAIL 0x00 ; keyboard / tty

; VGA
#define CLEARSCR  0x00
#define SETCURPOS 0x01
#define GETCURPOS 0x02
#define SETATTRIB 0x03
