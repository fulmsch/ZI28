#define RDWR_PIN PA0
#define CE_PIN PD3
#define SPKR_PIN PD5
#define WAIT_PIN PD6
#define NMI_PIN PA1
#define KBD_CLK_PIN PD2
#define KBD_DATA_PIN PD4


#include <avr/io.h>
#include <avr/pgmspace.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/cpufunc.h>

volatile uint8_t inputRegister;
volatile uint8_t outputRegister[4] = {0, 0, 0, 0};

volatile uint8_t receivedByte;
volatile uint8_t receivedFlag;

const uint8_t scancodes[128] PROGMEM = {
//    -0    -1    -2    -3    -4    -5    -6    -7
//    -8    -9    -A    -B    -C    -D    -E    -F
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, // 0-
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x10, 0x00, 0xFF,
	0xFF, 0xFF, 0x51, 0xFF, 0x53, 0x11, 0x01, 0xFF, // 1-
	0xFF, 0xFF, 0x31, 0x22, 0x21, 0x12, 0x02, 0xFF,
	0xFF, 0x33, 0x32, 0x23, 0x13, 0x04, 0x03, 0xFF, // 2-
	0xFF, 0x20, 0x34, 0x24, 0x15, 0x14, 0x05, 0xFF,
	0xFF, 0x36, 0x35, 0x26, 0x25, 0x16, 0x06, 0xFF, // 3-
	0xFF, 0xFF, 0x37, 0x27, 0x17, 0x07, 0x08, 0xFF,
	0xFF, 0x38, 0x28, 0x18, 0x19, 0x0A, 0x09, 0xFF, // 4-
	0xFF, 0x39, 0x3A, 0x29, 0x2A, 0x1A, 0x0B, 0xFF,
	0xFF, 0xFF, 0x2B, 0xFF, 0x1B, 0x0C, 0xFF, 0xFF, // 5-
	0xFF, 0x52, 0x2D, 0x1C, 0xFF, 0x1D, 0xFF, 0xFF,
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x0D, 0xFF, // 6-
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, // 7-
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
};

ISR(INT0_vect)
{
	static uint8_t buffer = 0;
	static uint8_t gBitIndex = 0;
	static uint8_t gFallingEdge = 1;
	
	if (gFallingEdge) {
		if (gBitIndex > 0 && gBitIndex <= 8) { // Ignore start, parity, and stop bits
			buffer >>= 1; // Make room for the next bit
			
			if (PIND & (1 << KBD_DATA_PIN)) {
				buffer |= 0x80; // Set the new bit
			}
		}
		
		// Now watch for rising edge of clock
		MCUCR |= (1<<ISC00);
		gFallingEdge = 0;
	} else {
		gBitIndex++;
		
		if (gBitIndex==11) {
			// We've received the whole packet
			receivedByte = buffer;
			receivedFlag = 1;
			
			// Clean up and prepare for next byte
			buffer = 0;
			gBitIndex = 0;
		}
		
		// Now watch for falling edge of clock
		MCUCR &= ~(1<<ISC00);
		gFallingEdge = 1;
	}
}

ISR(INT1_vect)
{
	uint8_t address = PIND & 0b00000011;
	PORTA |= (1 << NMI_PIN);
	if(PINA & (1 << RDWR_PIN)) {
		//Z80 read
		DDRB = 0xFF;
		PORTB = outputRegister[address];
		PORTD |= (1 << WAIT_PIN);
		//_NOP();
		DDRB = 0;
		PORTD &= !(1 << WAIT_PIN);
		PORTB = 0;
	}
	else {
		//Z80 write
		inputRegister = PINB;
		PORTD |= (1 << WAIT_PIN);
		_NOP();
		PORTD &= !(1 << WAIT_PIN);
		switch(address) {
			case 1:
				outputRegister[1] = inputRegister;
				break;
			case 2:
				TCCR0B = inputRegister & 0b00000111;
				break;
			case 3:
				OCR0A = inputRegister;
				break;
			default:
				break;
		}
	}
}

void processKeypress()
{
	static uint8_t releaseFlag = 0;

	if (!receivedFlag) return;

	receivedFlag = 0;
	uint8_t input = receivedByte;

	if (input == 0xF0) {
		releaseFlag = 1;
		return;
	}

	if (input > 0x7F) return;

	uint8_t scancode = pgm_read_byte(&(scancodes[input]));
	if (scancode == 0xFF) {
		releaseFlag = 0;
		return;
	}

	if (releaseFlag) {
		releaseFlag = 0;
		scancode |= (1 << 7);
	}

	outputRegister[0] = scancode;
	if (outputRegister[1]) {
		PORTA &= ~(1 << NMI_PIN);
	}
}

int main(void)
{
	// Enable pullups
	PORTD |= (1 << CE_PIN) | (1 << KBD_DATA_PIN) | (1 << KBD_CLK_PIN);
	
	DDRD |= (1 << SPKR_PIN) | (1 << WAIT_PIN);
	PORTA |= (1 << RDWR_PIN) | (1 << NMI_PIN);
	DDRA |= (1 << NMI_PIN);

	// Initialize INT0 (PS/2 clock)
	MCUCR |= (1 << ISC01);
	MCUCR &= ~(1 << ISC00);
	GIMSK |= (1 << INT0);

	// Initialize INT1 (Chip enable pin)
	MCUCR |= (1 << ISC11);
	GIMSK |= (1 << INT1);
	
	// Initialize Timer 0
	TCCR0A |= (1 << COM0B0) | (1 << WGM01);

	sei();
	
	// Main loop
	while (1) {
		processKeypress();
		// if (outputRegister[1]) {
		// 	outputRegister[0]++;
		// 	PORTA &= ~(1 << NMI_PIN);
		// 	_delay_ms(1000);
		// }
	}
	return(0);
}
