module mode_controller
 (
  input         i_clk,

  input [11:0]  i_cmd,
  input         i_cmd_avail,
  output reg    o_cmd_read,

  output        o_vram_write,
  output        o_vram_read,
  output [7:0]  o_vram_write_data,
  input [7:0]   i_vram_read_data,
  output [16:0] o_vram_addr,

  input [9:0]   i_xCounter,
  input [9:0]   i_yCounter,
  output [5:0]  o_color
  );

   localparam modes = 2;
   reg [$clog2(modes)-1:0] mode = 1;

   wire         vram_write    [modes-1:0];
   wire         vram_read     [modes-1:0];
   wire [7:0]   vram_data_out [modes-1:0];
   wire [16:0]  vram_addr     [modes-1:0];
   wire         cmd_read      [modes-1:0];
   wire [5:0]   color         [modes-1:0];

   assign o_vram_write      = vram_write[mode];
   assign o_vram_read       = vram_read[mode];
   assign o_vram_write_data = vram_data_out[mode];
   assign o_vram_addr       = vram_addr[mode];
   assign o_color           = color[mode];

   localparam
     cmd_state_idle = 0,
     cmd_state_processing = 1,
     cmd_state_waiting = 2;

   reg [1:0] cmd_state = 0;
   reg [11:0] cmd_buffer = 0;
   reg        cmd_avail = 0;

   //FIXME If the mode doesn't handle the commands, global register access is blocked
   //      It might make more sense to handle the global register before the fifo
   always @(posedge i_clk) begin
      case (cmd_state)
        cmd_state_idle: begin
           if (i_cmd_avail) begin
              cmd_state <= cmd_state_processing;
              o_cmd_read <= 1;
           end
        end

        cmd_state_processing: begin
           o_cmd_read <= 0;

           if (i_cmd[11:8] == 15) begin
              // Global register
              mode <= i_cmd[0];
              cmd_state <= cmd_state_idle;
           end else begin
              cmd_avail <= 1;
              cmd_buffer <= i_cmd;
              cmd_state <= cmd_state_waiting;
           end
        end

        cmd_state_waiting: begin
           if (cmd_read[mode]) begin
              cmd_avail <= 0;
              if (i_cmd_avail) begin
                 cmd_state <= cmd_state_processing;
                 o_cmd_read <= 1;
              end else begin
                 cmd_state <= cmd_state_idle;
              end
           end
        end
      endcase // case (cmd_state)
   end


   mode_0 mode_0_inst
     (
      .i_clk(i_clk),
      .i_enable(),

      .i_xCounter(i_xCounter),
      .i_yCounter(i_yCounter),

      .o_vram_write(vram_write[0]),
      .o_vram_read(vram_read[0]),
      .o_vram_data_out(vram_data_out[0]),
      .i_vram_data_in(i_vram_read_data),
      .o_vram_addr(vram_addr[0]),

      .i_cmd(cmd_buffer),
      .i_cmd_avail(cmd_avail),
      .o_cmd_read(cmd_read[0]),

      .o_color(color[0])
      );


   mode_1 mode_1_inst
     (
      .i_clk(i_clk),
      .i_enable(),

      .i_xCounter(i_xCounter),
      .i_yCounter(i_yCounter),

      .o_vram_write(vram_write[1]),
      .o_vram_read(vram_read[1]),
      .o_vram_data_out(vram_data_out[1]),
      .i_vram_data_in(i_vram_read_data),
      .o_vram_addr(vram_addr[1]),

      .i_cmd(cmd_buffer),
      .i_cmd_avail(cmd_avail),
      .o_cmd_read(cmd_read[1]),

      .o_color(color[1])
      );



endmodule // mode_controller
