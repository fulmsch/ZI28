module vga_top
  (
   input         CLK_25M,

   output        LED1,
   output        LED2,

   inout [7:0]   ZI28_D,
   input [3:0]   ZI28_A,
   input         N_ZI28_RD,
   input         N_ZI28_WR,
   output        N_ZI28_D_EN,
   output        N_ZI28_INT,

   inout [7:0]   VRAM_D,
   output [16:0] VRAM_A,
   output        N_VRAM_CE,
   output        N_VRAM_OE,
   output        N_VRAM_WE,

   output [1:0]  RED_B,
   output [1:0]  GREEN_B,
   output [1:0]  BLUE_B,
   output        N_VSYNC,
   output        N_HSYNC,
   output        N_VGA_EN
);



   reg [24:0] ledCounter = 25'b0;
   always @(posedge CLK_25M)
     begin
        ledCounter <= ledCounter + 1;
     end

   assign LED1 = ledCounter[24];
   assign LED2 = ~ledCounter[24];



// ---------- ZI28 Interface ------------------
   wire [15:0] cmd;
   wire        cmd_avail;
   wire        cmd_read;

   zi28_interface zi28_interface_inst
     (
      .i_clk(CLK_25M),
      .io_data(ZI28_D),
      .i_addr(ZI28_A),
      .i_n_read(N_ZI28_RD),
      .i_n_write(N_ZI28_WR),
      .o_n_data_en(N_ZI28_D_EN),
      .o_n_int(N_ZI28_INT),

      .i_cmd_read(cmd_read),
      .o_avail(cmd_avail),
      .o_cmd(cmd)
      );


// ---------- VRAM Interface ------------------
   wire        vram_write_en;
   wire        vram_read_en;
   wire [7:0]  vram_write_data;
   wire [7:0]  vram_read_data;
   wire [16:0] vram_addr;

   vram_mux vram_mux_inst
     (
      .i_clk(CLK_25M),

      .io_vram_data(VRAM_D),
      .o_vram_addr(VRAM_A),
      .o_n_ce(N_VRAM_CE),
      .o_n_we(N_VRAM_WE),
      .o_n_oe(N_VRAM_OE),

      .i_write_data(vram_write_data),
      .o_read_data(vram_read_data),
      .i_addr(vram_addr),
      .i_read(vram_read_en),
      .i_write(vram_write_en)
      );


// ---------- Video Generation ------------------
   wire [5:0] color;
   wire [9:0] xCounter, yCounter;
  
   video_generator video_generator_inst
     (
      .i_clk(CLK_25M),
      .i_color(color),

      .o_xCounter(xCounter),
      .o_yCounter(yCounter),

      .o_red(RED_B),
      .o_green(GREEN_B),
      .o_blue(BLUE_B),
      .o_n_vsync(N_VSYNC),
      .o_n_hsync(N_HSYNC),
      .o_n_vga_en(N_VGA_EN)
      );


   mode_controller mode_controller_inst
     (
      .i_clk(CLK_25M),

      .i_cmd(cmd[11:0]),
      .i_cmd_avail(cmd_avail),
      .o_cmd_read(cmd_read),

      .o_vram_write(vram_write_en),
      .o_vram_read(vram_read_en),
      .o_vram_write_data(vram_write_data),
      .i_vram_read_data(vram_read_data),
      .o_vram_addr(vram_addr),

      .i_xCounter(xCounter),
      .i_yCounter(yCounter),
      .o_color(color)
      );

endmodule
