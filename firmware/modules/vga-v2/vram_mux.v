module vram_mux
  (
   input         i_clk,

   inout [7:0]   io_vram_data,
   output [16:0] o_vram_addr,
   output        o_n_ce,
   output        o_n_we,
   output        o_n_oe,

   input [7:0]   i_write_data,
   output [7:0]  o_read_data,
   input [16:0]  i_addr,
   input         i_read,
   input         i_write

);

   wire vram_data_out_en;
   assign vram_data_out_en = !i_read && i_write;

   assign o_n_oe = !(i_read && !i_write);
   assign o_n_we = !(!i_read && i_write);
   assign o_n_ce = o_n_oe && o_n_we;

   assign o_vram_addr = i_addr;

   SB_IO
     #(
       .PIN_TYPE(6'b 1010_01),
       )
   vram_data_pins[7:0]
     (
      .PACKAGE_PIN(io_vram_data),
      .OUTPUT_ENABLE(vram_data_out_en),
      .D_OUT_0(i_write_data),
      .D_IN_0(o_read_data),
      );
  

  endmodule
