module zi28_interface
  (
   input         i_clk,

   input [3:0]   i_addr,
   inout [7:0]   io_data,

   input         i_n_read,
   input         i_n_write,

   output        o_n_data_en,
   output        o_n_int,

   input         i_cmd_read,
   output        o_avail,
   output [15:0] o_cmd
   );

   assign o_n_int = 1'b1;

   reg [7:0] zi28_data_out = 8'b0;
   wire [7:0] zi28_data_in;
   wire zi28_data_out_en;

   assign zi28_data_out_en = ~i_n_read;

   assign o_n_data_en = i_n_read & i_n_write;

   SB_IO
     #(
       .PIN_TYPE(6'b 1010_01),
       )
   zi28_data_pins[7:0]
     (
      .PACKAGE_PIN(io_data),
      .OUTPUT_ENABLE(zi28_data_out_en),
      .D_OUT_0(zi28_data_out),
      .D_IN_0(zi28_data_in),
      );

   reg  zi28_read = 1'b0;
   reg  zi28_read_prev = 1'b0;
   reg  zi28_read_buf = 1'b0;
   reg  zi28_write = 1'b0;
   reg  zi28_write_prev = 1'b0;
   reg  zi28_write_buf = 1'b0;

   reg [3:0] zi28_addr_buf = 4'b0;

   reg [15:0] cmd_fifo_in = 16'b0;

   always @(posedge i_clk) begin
      zi28_read <= zi28_read_prev;
      zi28_read_prev <= zi28_read_buf;
      zi28_read_buf <= ~i_n_read;

      zi28_write <= zi28_write_prev;
      zi28_write_prev <= zi28_write_buf;
      zi28_write_buf <= ~i_n_write;

      zi28_addr_buf <= i_addr;

      cmd_fifo_in <= {4'b0, zi28_addr_buf, zi28_data_in};
   end

   wire      cmd_fifo_write;
   assign cmd_fifo_write = zi28_write && !zi28_write_prev;

   wire      cmd_fifo_read;
   assign cmd_fifo_read = i_cmd_read;

   wire [15:0] cmd_fifo_out;
   wire        cmd_fifo_empty, cmd_fifo_full;

   fifo cmd_fifo
     (
      .i_clk(i_clk),
      .i_write_en(cmd_fifo_write),
      .i_read_en(cmd_fifo_read),

      .i_data(cmd_fifo_in),
      .o_data(cmd_fifo_out),

      .o_empty(cmd_fifo_empty),
      .o_full(cmd_fifo_full)
      );

   assign o_avail = !cmd_fifo_empty;
   assign o_cmd = cmd_fifo_out;

endmodule
