// 256x192 bitmap mode
module mode_1
  (
   input             i_clk,
   input             i_enable,

   input [9:0]       i_xCounter,
   input [9:0]       i_yCounter,

   output reg        o_vram_write = 1'b0,
   output reg        o_vram_read = 1'b0,
   output reg [7:0]  o_vram_data_out = 8'b0,
   input [7:0]       i_vram_data_in,
   output reg [16:0] o_vram_addr = 17'b0,

   input [11:0]      i_cmd,
   input             i_cmd_avail,
   output reg        o_cmd_read = 1'b0,

   output reg [5:0]  o_color = 6'b0
);

   reg [5:0]         border_color = 3;
   // reg [7:0]         scanline [0:255];

   reg [7:0]         index, lineIndex;
   reg [2:0]         state = 0;

   reg [7:0]         sram_read_addr = 0;
   reg [7:0]         sram_write_addr = 0;
   wire [7:0]        sram_read_data;
   reg [7:0]         sram_write_data = 0;
   reg               sram_write, sram_read;


   sram sram_inst
     (
      .i_clk(i_clk),
      .i_write_addr(sram_write_addr),
      .i_read_addr(sram_read_addr),
      .i_write(sram_write),
      .i_read(sram_read),
      .i_data(sram_write_data),
      .o_data(sram_read_data)
      );

   always @(posedge i_clk) begin
      case (state)
        0: if (i_yCounter == 47) state <= 1;

        1: begin
           if (i_yCounter == 432) state <= 0;
           else if (i_xCounter == 62) state <= 2;
        end

        2: state <= 3;

        3: begin
           if (i_xCounter >= 573) state <= 4;
           else state <= 2;
        end

        4: if (i_xCounter == 62) state <= 5;

        5: state <= 6;

        6: begin
           if (i_xCounter >= 573) state <= 1;
           else state <= 5;
        end
      endcase // case (state)
   end

   always @(posedge i_clk) begin
      // Default statements
      sram_write <= 0;
      sram_read <= 0;
      o_vram_read <= 0;
      o_vram_write <= 0;

      case (state)
        0: begin
           o_color <= 3;
           lineIndex <= -1;
        end
        1: begin
           o_color <= 15;
           index <= 0;
           lineIndex <= lineIndex + 1;
        end

        // odd lines
        2: begin
           o_vram_addr <= {1'b0, lineIndex, index};
           o_vram_read <= 1;

           sram_read <= 1;
           sram_read_addr <= index;
        end

        3: begin
           sram_write <= 1;
           sram_write_data <= i_vram_data_in;
           // o_color <= sram_read_data[5:0];
           o_color <= i_vram_data_in[5:0];
           index <= index + 1;
           // o_color <= index[5:0];
        end

        4: begin
           o_color <= 48;
           index <= 0;
        end

        // even lines
        5: begin
           sram_read <= 1;
           sram_read_addr <= index;
        end

        6: begin
           sram_write_data <= i_vram_data_in;
           o_color <= sram_read_data[5:0];
           index <= index + 1;
           // o_color <= index[5:0];
        end
      endcase // case (state)
   end


   always @(posedge i_clk) begin
      if (i_cmd_avail) begin
         o_cmd_read <= 1;
      end else begin
         o_cmd_read <= 0;
      end
   end

   // always @(posedge i_clk) begin
   //    if (i_xCounter < 64 || i_xCounter >= 576 ||
   //        i_yCounter < 48 || i_yCounter >= 432) begin
   //       o_color <= border_color;
   //    end
   // end


endmodule // mode_1

module sram #(parameter ADDR_WIDTH = 8, DATA_WIDTH = 8, DEPTH = 256)
   (
    input                       i_clk,
    input [ADDR_WIDTH-1:0]      i_write_addr,
    input [ADDR_WIDTH-1:0]      i_read_addr,
    input                       i_write,
    input                       i_read,
    input [DATA_WIDTH-1:0]      i_data,
    output reg [DATA_WIDTH-1:0] o_data
    );

   reg [DATA_WIDTH-1:0]         memory_array [0:DEPTH-1];

   always @ (posedge i_clk)
     begin
        if(i_write) begin
           memory_array[i_write_addr] <= i_data;
        end
        if(i_read) begin
           o_data <= memory_array[i_read_addr];
        end

     end
endmodule
