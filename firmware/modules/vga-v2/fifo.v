module fifo
  (
   input         i_clk,
   input         i_write_en,
   input         i_read_en,
  
   input [15:0]  i_data,
   output [15:0] o_data,

   output        o_empty,
   output        o_full
   );

   reg [15:0]    data_out;

   parameter addr_width = 8;
   reg [15:0]     mem [0:(2 ** addr_width)-1];

   reg [addr_width-1:0] read_addr = 0;
   reg [addr_width-1:0] write_addr = 0;
   reg [addr_width-1:0] count = 0;


   always @(posedge i_clk) begin
      if (i_write_en && !i_read_en)
        count <= count + 1;
      else if (!i_write_en && i_read_en)
        count <= count - 1;
      else
        count <= count;

      if (i_write_en) begin
         mem[write_addr] <= i_data;
         write_addr <= write_addr + 1;
      end
   end

   always @(negedge i_clk) begin
      if (i_read_en) begin
         data_out <= mem[read_addr];
         read_addr <= read_addr + 1;
      end
   end

   assign o_empty = count == 0;
   assign o_full = count == (2 ** addr_width) - 1;
   assign o_data = data_out;

endmodule
