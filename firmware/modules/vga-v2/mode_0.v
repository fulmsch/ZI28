// 80 x 40 Text mode
module mode_0
  (
   input             i_clk,
   input             i_enable,
  
   input [9:0]       i_xCounter,
   input [9:0]       i_yCounter,

   output reg        o_vram_write = 1'b0,
   output reg        o_vram_read = 1'b0,
   output reg [7:0]  o_vram_data_out = 8'b0,
   input [7:0]       i_vram_data_in,
   output reg [16:0] o_vram_addr = 17'b0,

   input [11:0]      i_cmd,
   input             i_cmd_avail,
   output reg        o_cmd_read = 1'b0,

   output reg [5:0]  o_color = 6'b0
);

   reg [24:0] blinkCounter = 25'b0;
   always @(posedge i_clk)
     begin
        blinkCounter <= blinkCounter + 1;
     end

   reg [16:0] write_addr = 17'b0;

   reg [1:0]  cmd_state = 2'b0;

   reg [7:0]  character;
   reg [7:0]  bitfield, next_bitfield;
   reg [5:0]  bg_color, fg_color, next_bg_color, next_fg_color;

   reg [6:0]  column_offset;
   reg [5:0]  line_offset;
   reg [6:0]  cursor_x;
   reg [5:0]  cursor_y;


   always @(posedge i_clk) begin
      if (i_xCounter > 791) begin // just a temporary value, maybe tweak it
         // o_vram_addr[8:0] <= 0;
         // o_vram_addr[16:9] <= i_yCounter[8:1] + 1;

         // vram_read_en <= 1;
         // vram_write_en <= 0;
      end else if (i_xCounter < 640) begin
         case (i_xCounter[2:0])
           0: o_color <= bitfield[7] ? fg_color : bg_color;
           1: begin
              o_color <= bitfield[6] ? fg_color : bg_color;
              o_vram_addr <= {3'b0, {6'b0, {i_xCounter[9:3], 1'b0} + {column_offset, 1'b0}}
                            + {i_yCounter[9:4], 8'b0} + {line_offset, 8'b0}};
              o_vram_read <= 1;
           end
           2: begin
              o_color <= bitfield[5] ? fg_color : bg_color;
              character <= i_vram_data_in;
              o_vram_addr <= o_vram_addr + 1;
           end
           3: begin
              o_color <= bitfield[4] ? fg_color : bg_color;
              next_fg_color[1] <= i_vram_data_in[0];
              next_fg_color[0] <= i_vram_data_in[0] && i_vram_data_in[3];
              next_fg_color[3] <= i_vram_data_in[1];
              next_fg_color[2] <= i_vram_data_in[1] && i_vram_data_in[3];
              next_fg_color[5] <= i_vram_data_in[2];
              next_fg_color[4] <= i_vram_data_in[2] && i_vram_data_in[3];
              next_bg_color[0] <= 0;
              next_bg_color[1] <= i_vram_data_in[4];
              next_bg_color[2] <= 0;
              next_bg_color[3] <= i_vram_data_in[5];
              next_bg_color[4] <= 0;
              next_bg_color[5] <= i_vram_data_in[6];

              o_vram_addr <= 17'h4000 + {5'b0, character, 4'b0} + {13'b0, i_yCounter[3:0]};
           end
           4: begin
              o_color <= bitfield[3] ? fg_color : bg_color;
              next_bitfield <= i_vram_data_in;
              o_vram_read <= 0;
           end
           5: o_color <= bitfield[2] ? fg_color : bg_color;
           6: o_color <= bitfield[1] ? fg_color : bg_color;
           7: begin
              o_color <= bitfield[0] ? fg_color : bg_color;
              bitfield <= next_bitfield;
              if (blinkCounter[24] &&
                  i_xCounter[9:3] == cursor_x &&
                  i_yCounter[9:4] == cursor_y)
                begin
                   bg_color <= next_fg_color;
                   fg_color <= next_bg_color;
                end else begin
                   fg_color <= next_fg_color;
                   bg_color <= next_bg_color;
                end
           end
         endcase
      end else if (i_xCounter < 780 || cmd_state != 0) begin
         // Default statements
         o_vram_read <= 0;
         o_vram_write <= 0;
         o_cmd_read <= 0;

         if (i_cmd_avail && cmd_state == 0) begin
            o_cmd_read <= 1;

            if (i_cmd[11:8] == 0) begin
               o_vram_data_out <= i_cmd[7:0];
               o_vram_addr <= write_addr;
               write_addr <= write_addr + 1;
               o_vram_write <= 1;
               cmd_state <= 2;
            end else begin
               case (i_cmd[11:8])
                 1: write_addr[7:0]  <= i_cmd[7:0];
                 2: write_addr[15:8] <= i_cmd[7:0];
                 3: write_addr[16]   <= i_cmd[0];
                 4: column_offset    <= i_cmd[6:0];
                 5: line_offset      <= i_cmd[5:0];
                 6: cursor_x         <= i_cmd[6:0];
                 7: cursor_y         <= i_cmd[5:0];
               endcase
               cmd_state <= 0;
            end
         end else if (cmd_state == 2) begin
            o_vram_write <= 0;
            cmd_state <= 0;
         end
      end
   end // always @ (posedge i_clk)

endmodule // mode_0
