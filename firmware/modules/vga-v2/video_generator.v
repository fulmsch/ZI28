module video_generator
 (
  input        i_clk,
  input [5:0]  i_color,

  output [9:0] o_xCounter,
  output [9:0] o_yCounter,

  output [1:0] o_red,
  output [1:0] o_green,
  output [1:0] o_blue,
  output       o_n_vsync,
  output       o_n_hsync,
  output       o_n_vga_en

);

   reg [9:0] xCounter = 10'b0;
   reg [9:0] yCounter = 10'b0;

   assign {o_xCounter, o_yCounter} = {xCounter, yCounter};

   assign o_n_vga_en = 1'b0;

   always @(posedge i_clk) begin
      if (xCounter == 800)
        begin
           xCounter <= 10'b0;
           yCounter <= yCounter + 1;
        end
      else
        begin
           xCounter <= xCounter + 1;
           yCounter <= yCounter;
        end
      if (yCounter == 525)
        yCounter <= 10'b0;
   end

   assign o_n_hsync = !(xCounter >= 640 + 16 && xCounter < 640 + 16 + 96);
   assign o_n_vsync = !(yCounter >= 480 + 10 && yCounter < 480 + 10 + 2);

   wire vis;
   assign vis = (xCounter < 640 && yCounter < 480);

   assign {o_red, o_green, o_blue} = vis ? i_color : 6'b0;

endmodule // video_generator
