#include <stdio.h>
#include <stdbool.h>
#include <lua.h>
#include <lauxlib.h>
#include <string.h>
#include <errno.h>
#include <SDL.h>

#define LUA_LIB


struct vgamod {
	bool isVisible;
	uint8_t vram[0x10000];
	uint32_t address;
	uint8_t columnOffset, lineOffset;
	uint8_t cursorX, cursorY;
};


static int vga_read(lua_State *L) {
	lua_settop(L, 2);
	struct vgamod *module = lua_touserdata(L, 1);
	int isnum;
	unsigned int address = lua_tointegerx(L, 2, &isnum);
	if (module == NULL || !isnum) {
		return luaL_error(L, "Invalid argument");
	}

	unsigned char data = 0xff;

	lua_pushinteger(L, data);
	return 1;
}

static int vga_write(lua_State *L) {
	// 0: VRAM
	// 1: Addr[7:0]
	// 2: Addr[15:8]
	// 3: Addr[16]
	// 4: Column offset
	// 5: Line offset
	// 6: Cursor X
	// 7: Cursor Y
	struct vgamod *module = lua_touserdata(L, 1);
	int isnum1, isnum2;
	unsigned int address = lua_tointegerx(L, 2, &isnum1);
	unsigned char data = lua_tointegerx(L, 3, &isnum2);
	if (module == NULL || !isnum1 || !isnum2) {
		return luaL_error(L, "Invalid argument");
	}

	switch (address) {
		case 0:
			module->vram[module->address] = data;
			module->address += 1;
			break;
		case 1:
			module->address = (module->address & 0x1ff00) | (uint32_t)data;
			break;
		case 2:
			module->address = (module->address & 0x100ff) | (uint32_t)(data << 8);
			break;
		case 3:
			module->address = (module->address & 0x0ffff) | (uint32_t)((data << 16) & 1);
			break;
		case 4:
			module->columnOffset = data & 0x7f;
			break;
		case 5:
			module->lineOffset = data & 0x3f;
			break;
		case 6:
			module->cursorX = data & 0x7f;
			break;
		case 7:
			module->cursorY = data & 0x3f;
			break;
	}
	return 0;
}

static int vga_hideWindow(lua_State *L) {
	struct vgamod *module = lua_touserdata(L, 1);
	module->isVisible = false;
	return 0;
}

static int vga_showWindow(lua_State *L) {
	struct vgamod *module = lua_touserdata(L, 1);
	module->isVisible = true;
	return 0;
}

static int vgaMain(void *data) {
	struct vgamod *module = (struct vgamod *)data;
	bool windowVisible = true;
	bool done = false;
	SDL_Event event;
	SDL_Window *window;
	SDL_Renderer *renderer;
	unsigned int x, y;

	for (int i = 0; i < sizeof(module->vram); i++) {
		module->vram[i] = 0;
	}
	module->columnOffset = 0;
	module->lineOffset = 0;
	module->cursorX = 0;
	module->cursorY = 0;

#define WIDTH 640
#define HEIGHT 480

	SDL_CreateWindowAndRenderer(WIDTH, HEIGHT, 0, &window, &renderer);
	SDL_RenderSetLogicalSize(renderer, WIDTH, HEIGHT);
	SDL_RenderPresent(renderer);

	while(!done) {
		while(SDL_PollEvent(&event)) {
			if (event.type == SDL_QUIT) {
				module->isVisible = false;
				windowVisible = false;;
				SDL_HideWindow(window);
			}
		}
		if (windowVisible != module->isVisible) {
			windowVisible = module->isVisible;
			if (windowVisible) {
				SDL_ShowWindow(window);
			} else {
				SDL_HideWindow(window);
			}
		}
		if (!windowVisible) {
			SDL_Delay(16);
		}

		uint8_t bitfield = 0;
		struct color {
			uint8_t r, g, b;
		};
		struct color fgColor = {0, 0, 0};
		struct color bgColor = {0, 0, 0};

		for (y = 0; y < HEIGHT; y++) {
			for (x = 0; x < WIDTH; x++) {
				if (x % 8 == 0) {
					uint32_t address = (((x / 8) + module->columnOffset) << 1) +
						(((y / 16) + module->lineOffset) << 8);
					address &= 0x3fff;
					uint8_t character = module->vram[address];
					uint8_t color = module->vram[address + 1];

					fgColor.r = !!(color & 0x04) * ((color & 0x08) ? 255 : 169);
					fgColor.g = !!(color & 0x02) * ((color & 0x08) ? 255 : 169);
					fgColor.b = !!(color & 0x01) * ((color & 0x08) ? 255 : 169);

					bgColor.r = !!(color & 0x40) * 169;
					bgColor.g = !!(color & 0x20) * 169;
					bgColor.b = !!(color & 0x10) * 169;

					// TODO implement cursor blinking
					if ((x / 8 == module->cursorX) && (y / 16 == module->cursorY)) {
						struct color buf = fgColor;
						fgColor = bgColor;
						bgColor = buf;
					}

					address = 0x4000 + (character << 4) + (y % 16);
					bitfield = module->vram[address];
				}

				struct color dotColor;
				if (bitfield & (1 << (7 - x % 8))) {
					dotColor = fgColor;
				} else {
					dotColor = bgColor;
				}

				SDL_SetRenderDrawColor(renderer, dotColor.r, dotColor.g, dotColor.b, 255);
				SDL_RenderDrawPoint(renderer, x, y);
			}
		}
		SDL_RenderPresent(renderer);
		SDL_Delay(16);
	}
	SDL_DestroyWindow(window);
	return 0;
}

static const luaL_Reg vga_interface[] = {
	{"read",   vga_read},
	{"write",  vga_write},
	{"show",   vga_showWindow},
	{"hide",   vga_hideWindow},
	{NULL, NULL}
};



static int vga_new(lua_State *L) {
	// Constructor
	SDL_Thread *vgaThread;
	lua_settop(L, 0);
	struct vgamod *module = lua_newuserdata(L, sizeof(struct vgamod));
	module->isVisible = true;
	vgaThread = SDL_CreateThread(vgaMain, "vga", (void *)module);
	lua_createtable(L, 0, 1); //last arg: number of entries
	luaL_newlib(L, vga_interface);
	lua_setfield(L, 2, "__index");
	lua_setmetatable(L, 1);
	return 1;
}

static const luaL_Reg module_vga[] = {
	{"new", vga_new},
	{NULL, NULL}
};

LUAMOD_API int luaopen_vga(lua_State *L) {
  luaL_newlib(L, module_vga);
  return 1;
}
