#include <sys/os.h>
#include <sys/ioctl.h>
#include <errno.h>

int ioctl(int fd, unsigned char request, int parameter) {
	#asm
	ld hl, 6
	add hl, sp
	ld a, (hl) ;fd
	pop bc ;return address
	pop de ;parameter
	pop hl ;request
	ld h, l
	inc sp
	inc sp ;clear fd
	push bc ;return address

	ld c, SYS_ioctl
	rst RST_syscall
	;a = errno
	cp 0
	jr nz, error
	; return 0
	ld h, a
	ld l, a
	ret

error:
	ld hl, _errno
	ld (hl), 0
	inc hl
	ld (hl), a ;errno
	ld hl, -1
	ret
	#endasm
}
