EESchema Schematic File Version 4
LIBS:arduino-adapter-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCU_Module:Arduino_Nano_v3.x A1
U 1 1 5CF953D8
P 2750 3450
F 0 "A1" H 2400 4400 50  0000 C CNN
F 1 "Arduino_Nano_v3.x" V 2750 3450 50  0000 C CNN
F 2 "Module:Arduino_Nano" H 2900 2500 50  0001 L CNN
F 3 "http://www.mouser.com/pdfdocs/Gravitech_Arduino_Nano3_0.pdf" H 2750 2450 50  0001 C CNN
	1    2750 3450
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x12_Odd_Even J1
U 1 1 5CF978DB
P 5550 3100
F 0 "J1" H 5600 3817 50  0000 C CNN
F 1 "Conn_02x12_Odd_Even" H 5600 3726 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x12_P2.54mm_Vertical" H 5550 3100 50  0001 C CNN
F 3 "~" H 5550 3100 50  0001 C CNN
	1    5550 3100
	-1   0    0    -1  
$EndComp
$Comp
L power:+5V #PWR01
U 1 1 5CF99E8A
P 5250 2600
F 0 "#PWR01" H 5250 2450 50  0001 C CNN
F 1 "+5V" V 5265 2728 50  0000 L CNN
F 2 "" H 5250 2600 50  0001 C CNN
F 3 "" H 5250 2600 50  0001 C CNN
	1    5250 2600
	0    -1   -1   0   
$EndComp
$Comp
L power:+5V #PWR03
U 1 1 5CF9A5C8
P 5750 2600
F 0 "#PWR03" H 5750 2450 50  0001 C CNN
F 1 "+5V" V 5765 2728 50  0000 L CNN
F 2 "" H 5750 2600 50  0001 C CNN
F 3 "" H 5750 2600 50  0001 C CNN
	1    5750 2600
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR02
U 1 1 5CF9B44E
P 5250 2700
F 0 "#PWR02" H 5250 2450 50  0001 C CNN
F 1 "GND" V 5255 2572 50  0000 R CNN
F 2 "" H 5250 2700 50  0001 C CNN
F 3 "" H 5250 2700 50  0001 C CNN
	1    5250 2700
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR04
U 1 1 5CF9BB82
P 5750 2700
F 0 "#PWR04" H 5750 2450 50  0001 C CNN
F 1 "GND" V 5755 2572 50  0000 R CNN
F 2 "" H 5750 2700 50  0001 C CNN
F 3 "" H 5750 2700 50  0001 C CNN
	1    5750 2700
	0    -1   -1   0   
$EndComp
NoConn ~ 5750 3700
Text Label 6150 2900 2    50   ~ 0
D3
Text Label 6150 3000 2    50   ~ 0
D5
Text Label 6150 3100 2    50   ~ 0
D7
Wire Wire Line
	4850 2800 5250 2800
Wire Wire Line
	4850 2900 5250 2900
Wire Wire Line
	4850 3000 5250 3000
Wire Wire Line
	4850 3100 5250 3100
Wire Wire Line
	5750 2800 6150 2800
Wire Wire Line
	5750 2900 6150 2900
Wire Wire Line
	5750 3000 6150 3000
Wire Wire Line
	5750 3100 6150 3100
Wire Wire Line
	5750 3200 6150 3200
Wire Wire Line
	5750 3300 6150 3300
Wire Wire Line
	5750 3400 6150 3400
Wire Wire Line
	5750 3500 6150 3500
Wire Wire Line
	5750 3600 6150 3600
Wire Wire Line
	5250 3200 4850 3200
Wire Wire Line
	5250 3300 4850 3300
Wire Wire Line
	5250 3400 4850 3400
Wire Wire Line
	5250 3500 4850 3500
Wire Wire Line
	5250 3600 4850 3600
Text Label 6150 2800 2    50   ~ 0
D1
Text Label 4850 3100 0    50   ~ 0
D6
Text Label 4850 3000 0    50   ~ 0
D4
Text Label 4850 2900 0    50   ~ 0
D2
Wire Wire Line
	5250 3700 4850 3700
Text Label 4850 2800 0    50   ~ 0
D0
Text Label 4850 3200 0    50   ~ 0
A0
Text Label 4850 3300 0    50   ~ 0
A2
Text Label 4850 3400 0    50   ~ 0
~RST
Text Label 4850 3500 0    50   ~ 0
~INT1
Text Label 4850 3600 0    50   ~ 0
~RD1
Text Label 4850 3700 0    50   ~ 0
ID1
Text Label 6150 3200 2    50   ~ 0
A1
Text Label 6150 3300 2    50   ~ 0
A3
Text Label 6150 3400 2    50   ~ 0
~WAIT
Text Label 6150 3500 2    50   ~ 0
~IORQ1
Text Label 6150 3600 2    50   ~ 0
~WR1
$Comp
L Connector_Generic:Conn_02x12_Odd_Even J2
U 1 1 5CFA8324
P 7050 3100
F 0 "J2" H 7100 3817 50  0000 C CNN
F 1 "Conn_02x12_Odd_Even" H 7100 3726 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x12_P2.54mm_Vertical" H 7050 3100 50  0001 C CNN
F 3 "~" H 7050 3100 50  0001 C CNN
	1    7050 3100
	-1   0    0    -1  
$EndComp
$Comp
L power:+5V #PWR05
U 1 1 5CFA832A
P 6750 2600
F 0 "#PWR05" H 6750 2450 50  0001 C CNN
F 1 "+5V" V 6765 2728 50  0000 L CNN
F 2 "" H 6750 2600 50  0001 C CNN
F 3 "" H 6750 2600 50  0001 C CNN
	1    6750 2600
	0    -1   -1   0   
$EndComp
$Comp
L power:+5V #PWR07
U 1 1 5CFA8330
P 7250 2600
F 0 "#PWR07" H 7250 2450 50  0001 C CNN
F 1 "+5V" V 7265 2728 50  0000 L CNN
F 2 "" H 7250 2600 50  0001 C CNN
F 3 "" H 7250 2600 50  0001 C CNN
	1    7250 2600
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR06
U 1 1 5CFA8336
P 6750 2700
F 0 "#PWR06" H 6750 2450 50  0001 C CNN
F 1 "GND" V 6755 2572 50  0000 R CNN
F 2 "" H 6750 2700 50  0001 C CNN
F 3 "" H 6750 2700 50  0001 C CNN
	1    6750 2700
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR08
U 1 1 5CFA833C
P 7250 2700
F 0 "#PWR08" H 7250 2450 50  0001 C CNN
F 1 "GND" V 7255 2572 50  0000 R CNN
F 2 "" H 7250 2700 50  0001 C CNN
F 3 "" H 7250 2700 50  0001 C CNN
	1    7250 2700
	0    -1   -1   0   
$EndComp
NoConn ~ 7250 3700
Text Label 7650 2900 2    50   ~ 0
D3
Text Label 7650 3000 2    50   ~ 0
D5
Text Label 7650 3100 2    50   ~ 0
D7
Wire Wire Line
	6350 2800 6750 2800
Wire Wire Line
	6350 2900 6750 2900
Wire Wire Line
	6350 3000 6750 3000
Wire Wire Line
	6350 3100 6750 3100
Wire Wire Line
	7250 2800 7650 2800
Wire Wire Line
	7250 2900 7650 2900
Wire Wire Line
	7250 3000 7650 3000
Wire Wire Line
	7250 3100 7650 3100
Wire Wire Line
	7250 3200 7650 3200
Wire Wire Line
	7250 3300 7650 3300
Wire Wire Line
	7250 3400 7650 3400
Wire Wire Line
	7250 3500 7650 3500
Wire Wire Line
	7250 3600 7650 3600
Wire Wire Line
	6750 3200 6350 3200
Wire Wire Line
	6750 3300 6350 3300
Wire Wire Line
	6750 3400 6350 3400
Wire Wire Line
	6750 3500 6350 3500
Wire Wire Line
	6750 3600 6350 3600
Text Label 7650 2800 2    50   ~ 0
D1
Text Label 6350 3100 0    50   ~ 0
D6
Text Label 6350 3000 0    50   ~ 0
D4
Text Label 6350 2900 0    50   ~ 0
D2
Wire Wire Line
	6750 3700 6350 3700
Text Label 6350 2800 0    50   ~ 0
D0
Text Label 6350 3200 0    50   ~ 0
A0
Text Label 6350 3300 0    50   ~ 0
A2
Text Label 6350 3400 0    50   ~ 0
~RST
Text Label 6350 3500 0    50   ~ 0
~INT2
Text Label 6350 3600 0    50   ~ 0
~RD2
Text Label 6350 3700 0    50   ~ 0
ID2
Text Label 7650 3200 2    50   ~ 0
A1
Text Label 7650 3300 2    50   ~ 0
A3
Text Label 7650 3400 2    50   ~ 0
~WAIT
Text Label 7650 3500 2    50   ~ 0
~IORQ2
Text Label 7650 3600 2    50   ~ 0
~WR2
$Comp
L power:+5V #PWR0101
U 1 1 5CF8F948
P 2950 2450
F 0 "#PWR0101" H 2950 2300 50  0001 C CNN
F 1 "+5V" V 2965 2578 50  0000 L CNN
F 2 "" H 2950 2450 50  0001 C CNN
F 3 "" H 2950 2450 50  0001 C CNN
	1    2950 2450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 5CF91CE4
P 2850 4450
F 0 "#PWR0102" H 2850 4200 50  0001 C CNN
F 1 "GND" H 2900 4250 50  0000 C CNN
F 2 "" H 2850 4450 50  0001 C CNN
F 3 "" H 2850 4450 50  0001 C CNN
	1    2850 4450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 5CF915EC
P 2750 4450
F 0 "#PWR0103" H 2750 4200 50  0001 C CNN
F 1 "GND" H 2700 4250 50  0000 C CNN
F 2 "" H 2750 4450 50  0001 C CNN
F 3 "" H 2750 4450 50  0001 C CNN
	1    2750 4450
	1    0    0    -1  
$EndComp
NoConn ~ 2250 2850
NoConn ~ 2250 2950
NoConn ~ 3250 2850
NoConn ~ 3250 2950
NoConn ~ 3250 3250
Text Label 3500 3450 2    50   ~ 0
D0
Wire Wire Line
	3250 3450 3500 3450
Wire Wire Line
	3250 3650 3500 3650
Wire Wire Line
	3250 3750 3500 3750
Wire Wire Line
	3250 3850 3500 3850
Wire Wire Line
	3250 3950 3500 3950
NoConn ~ 3250 4050
NoConn ~ 3250 4150
Text Label 3500 3650 2    50   ~ 0
D2
Text Label 3500 3750 2    50   ~ 0
D3
Text Label 3500 3850 2    50   ~ 0
D4
Text Label 3500 3950 2    50   ~ 0
D5
Wire Wire Line
	2250 4150 2000 4150
Text Label 2000 4150 0    50   ~ 0
D6
Wire Wire Line
	2250 4050 2000 4050
Text Label 3500 3550 2    50   ~ 0
D1
Wire Wire Line
	3250 3550 3500 3550
Text Label 2000 4050 0    50   ~ 0
D7
$EndSCHEMATC
