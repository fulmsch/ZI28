#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <sys/ioctl.h>

#include "main.h"
#include "game.h"
#include "ui.h"
#include "vt100.h"

static char key, status;

void quit(int ret)
{
	vt100_set_cursor(25, 1);
	vt100_show_cursor();
	exit(ret);
}

void handleKey(char key)
{
	switch (key) {
		case 0x03:
		case 'q':
		case EOF:
			quit(0);
		case 'h':
		case 'a':
			moveLeft();
			break;
		case 'j':
		case 's':
			softDrop();
			break;
		case 'k':
		case 'w':
			rotateRight();
			break;
		case 'l':
		case 'd':
			moveRight();
			break;
		case ' ':
			hardDrop();
			break;
		case 'p':
			while (getchar() != 'p');
		default:
			break;
	}
}

void keyPoll(void)
{
	uint8_t dataAvail = 0;
	int count = speed[level > 30 ? 30 : level];

	while (count --> 0) {
		t_delay(80000); //~10ms

		ioctl(STDIN_FILENO, GETDAVAIL, (int)(&dataAvail));
		if (dataAvail) {
			read(STDIN_FILENO, &key, 1);
			handleKey(key);
		}
	}
}


int main(int argc, char **argv) {
	randomize();

	initGame();

	while (1) {
		keyPoll();
		softDrop();
	}
	quit(0);
}
