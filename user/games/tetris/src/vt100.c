#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>

int vt100_get_status() {
	return 0;
}

void vt100_clear_screen() {
	ioctl(STDOUT_FILENO, CLEARSCR, 0);
	ioctl(STDOUT_FILENO, SETCURPOS, 0);
}

void vt100_set_cursor(int row, int column) {
	row -= 1; row &= 0xff;
	column -= 1; column &= 0xff;
	ioctl(STDOUT_FILENO, SETCURPOS, (row << 8) | column);
}

void vt100_hide_cursor() {
}

void vt100_show_cursor() {
}
